
#include "FlockingManager.h"
#include "Engine.h"

#include "FlockingElement.h"
#include "UnrealMathUtility.h"


AFlockingManager::AFlockingManager()
{
	PrimaryActorTick.bCanEverTick = true;
	_flockElementAmount = 30;
	_raycastDistance = 2000;
	_avoidDistance = 250;
	_rotationSpeed = 25;
	_flockingLimit = FVector(10000, 10000, 5000);
	_neightbourDistance = 2000;
	_newGoalLocationChance = 20;
	_newGoalLocationChangeFrequency = 5;
	_applyRulesChance = 40;

	_speed = FVector2D(250, 350);
	_newSpeedChance = 50;
	_newSpeedChangeFrequency = 2.0f;
}

void AFlockingManager::BeginPlay()
{
	Super::BeginPlay();
	SetFlockingBound();
	SpawnFlockElements(_flockElementAmount);
}

void AFlockingManager::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	_newGoalLocationTimer -= deltaTime;
	SetNewGoalLocation();
}

void AFlockingManager::SetFlockingBound()
{
	_flockingBound.Min = GetActorLocation() - _flockingLimit / 2;
	_flockingBound.Max = GetActorLocation() + _flockingLimit / 2;
}

void AFlockingManager::SpawnFlockElements(int amount)
{
	for(int i = 0; i < amount; ++i)
	{
		_flockElements.Add(SpawnFlockElement());
	}
}

AFlockingElement* AFlockingManager::SpawnFlockElement()
{
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	spawnParams.Instigator = Instigator; 
	AFlockingElement* newElement = GetWorld()->SpawnActor<AFlockingElement>(_flockingElementBPs[FMath::RandRange(0, _flockingElementBPs.Num() -1)], GetRandomPointInBound(), GetActorRotation(), spawnParams);
	newElement->Init(this);
	UE_LOG(LogTemp, Log, TEXT("%s spawned"), *newElement->GetName());
	return newElement;
}

FVector2D AFlockingManager::GetSpeed() const
{
	return _speed;
}

float AFlockingManager::GetNeightbourDistance() const
{
	return _neightbourDistance;
}

float AFlockingManager::GetRotationSpeed() const
{
	return _rotationSpeed / 180;
}

float AFlockingManager::GetRaycastDistance() const
{
	return _raycastDistance;
}

float AFlockingManager::GetAvoidDistance() const
{
	return _avoidDistance;
}

bool AFlockingManager::IsInBound(const FVector& position) const
{
	return _flockingBound.IsInside(position);
}

TArray<AFlockingElement*> AFlockingManager::GetElements() const
{
	return _flockElements;
}

FVector AFlockingManager::GetGoalLocation() const
{
	return _goalLocation;
}

void AFlockingManager::SetNewGoalLocation()
{
	if(_newGoalLocationTimer > 0)
		return;
	_newGoalLocationTimer = _newGoalLocationChangeFrequency;
	if(FMath::RandRange(0, 100) < _newGoalLocationChance)
	{
		_goalLocation = GetRandomPointInBound();
	}
}

FVector AFlockingManager::GetRandomPointInBound() const
{
	return GetActorLocation() + FVector(
		FMath::RandRange(-_flockingLimit.X / 2, _flockingLimit.X / 2),
		FMath::RandRange(-_flockingLimit.Y / 2, _flockingLimit.Y / 2),
		FMath::RandRange(-_flockingLimit.Z / 2, _flockingLimit.Z / 2));
}

int AFlockingManager::GetApplyRulesChance() const
{
	return _applyRulesChance;
}

float AFlockingManager::GetNewSpeedChangeFrequency() const
{
	return _newSpeedChangeFrequency;
}

int AFlockingManager::GetNewSpeedChance() const
{
	return _newSpeedChance;
}