
#include "FlockingElement.h"
#include "Engine.h"

#include "FlockingManager.h"

AFlockingElement::AFlockingElement()
{
	PrimaryActorTick.bCanEverTick = true;
	_traceParams = new FCollisionQueryParams(TEXT("FlockingElementRaycast"), false, this);
}

AFlockingElement::~AFlockingElement()
{
	delete(_traceParams);
}

void AFlockingElement::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	_newSpeedTimer -= deltaTime;
	MoveForward(deltaTime);
	bool turning = IsTurning(deltaTime);
	if(!turning)
	{
		if(_newSpeedTimer < 0)
		{
			_newSpeedTimer = _flockingManager->GetNewSpeedChangeFrequency();
			if(FMath::RandRange(0, 100) < _flockingManager->GetNewSpeedChance())
				SetNewSpeed();
		}
		if(FMath::RandRange(0, 100) < _flockingManager->GetApplyRulesChance())
			ApplyRules(deltaTime);
	}
}

void AFlockingElement::Init(AFlockingManager* flockingManager)
{
	_flockingManager = flockingManager;
	SetNewSpeed();
}

bool AFlockingElement::IsTurning(float deltaTime)
{
	if(!_flockingManager->IsInBound(GetActorLocation()))
	{
		FVector direction = _flockingManager->GetActorLocation() - GetActorLocation();
		Rotate(direction, deltaTime);
		return true;
	}
	FHitResult hit;
	if(CheckRaycast(&hit))
	{
		auto a = hit.GetActor()->GetName();
		FVector direction = FMath::GetReflectionVector(GetActorForwardVector(), hit.Normal);
		Rotate(direction, deltaTime);
		return true;
	}
	return false;
}

bool AFlockingElement::CheckRaycast(FHitResult *hit)
{
	auto startPos = GetActorLocation()  + GetActorForwardVector() * _raycastStartPosOffsetMultplier;
	auto endPos = startPos + GetActorForwardVector() * _flockingManager->GetRaycastDistance();
	return GetWorld()->LineTraceSingleByChannel(*hit, startPos, endPos, ECC_Visibility, _traceParams);
}

void AFlockingElement::Rotate(FVector& direction, float deltaTime)
{
	direction.Normalize();
	SetActorRotation(FQuat::Slerp(GetActorRotation().Quaternion(), direction.ToOrientationQuat(), _flockingManager->GetRotationSpeed() * deltaTime));
}

void AFlockingElement::MoveForward(float deltaTime)
{
	SetActorLocation(GetActorLocation() + GetActorForwardVector()* deltaTime * _speed);
}

void AFlockingElement::SetNewSpeed()
{
	auto speedLimit = _flockingManager->GetSpeed();
	_speed =  FMath::RandRange(speedLimit.X, speedLimit.Y);
}

void AFlockingElement::ApplyRules(float deltaTime)
{
	FVector center = FVector::ZeroVector;
	FVector avoid = FVector::ZeroVector;

	float groupSpeed = 0.0f;
	int groupSize = 0;

	for(auto anotherElement : _flockingManager->GetElements())
	{
		float distance = FVector::Distance(anotherElement->GetActorLocation(), GetActorLocation());
		if(distance <= _flockingManager->GetNeightbourDistance())
		{
			center += anotherElement->GetActorLocation();
			++groupSize;
			if(distance <= _flockingManager->GetAvoidDistance())
			{
				avoid += GetActorLocation() - anotherElement->GetActorLocation();
			}
			groupSpeed += anotherElement->GetSpeed();
		}
	}

	if(groupSize > 0)
	{
		center = center / groupSize + (_flockingManager->GetGoalLocation() - GetActorLocation());
		_speed = groupSpeed / groupSize;

		FVector direction = center + avoid - GetActorLocation();
		if(direction != FVector::ZeroVector)
			Rotate(direction, deltaTime);
	}
}

float AFlockingElement::GetSpeed()
{
	return _speed;
}
