// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapPlanner.h"

#include <limits>
#include "GoapAction.h"

void DeleteAllNotes(TArray<Node*>*);
void RemoveAllElementWithKey(TSet<TPair<FString, void*>>*, TPair<FString, void*>);

GoapPlanner::GoapPlanner()
{
}

GoapPlanner::~GoapPlanner()
{
}

bool GoapPlanner::Plan(TQueue<AGoapAction*>* plan, TSet<AGoapAction*>* availableActions, TSet<TPair<FString, void*>>* worldStates, TSet<TPair<FString, void*>>* goals)
{
	TSet<AGoapAction*> usableActions;
	ResetActionsAndGetUsableActions(&usableActions, availableActions);

	auto leaves = new TArray<Node*>();
	auto start = new Node(nullptr, 0, worldStates, nullptr);
	if(!BuildGraph(start, leaves, &usableActions, goals))
	{
		UE_LOG(LogTemp, Warning, TEXT("NO PLAN"));
		return false;
	}
	Node* cheapest = nullptr;
	GetCheapestNode(cheapest, leaves);
	CreatePlan(plan, cheapest);

	delete(start);
	DeleteAllNotes(leaves);
	delete(leaves);

	return true;
}

void GoapPlanner::CreatePlan(TQueue<AGoapAction*>* plan, Node* node)
{
	TArray<AGoapAction*> reversePlan;
	while(node != nullptr)
	{
		if(node->Action != nullptr)
			reversePlan.Add(node->Action);
		node = node->Parent;
	}
	for(int32 i = reversePlan.Num() - 1; i >= 0; --i)
	{
		plan->Enqueue(reversePlan[i]);
	}
}

void GoapPlanner::GetCheapestNode(Node* cheapest, TArray<Node*>* nodes)
{
	cheapest = new Node();
	for(auto leaf : *nodes)
	{
		if(leaf->RunningCost < cheapest->RunningCost)
			cheapest = leaf;
	}
}

void GoapPlanner::ResetActionsAndGetUsableActions(TSet<AGoapAction*>* usableActions, TSet<AGoapAction*>* availableActions)
{
	for(auto a : *availableActions)
	{
		a->Reset();
		if(a->CheckProceduralPrecondition())
			usableActions->Add(a);
	}
}

bool GoapPlanner::BuildGraph(Node* parent, TArray<Node*>* leaves, TSet<AGoapAction*>* usableActions, TSet<TPair<FString, void*>>* goals)
{
	bool foundOne = false;
	for(auto action : *usableActions)
	{
		if(!InState(action->GetPreconditions(), parent->State))
			continue;

		TSet<TPair<FString, void*>> currentState;
		PopulateState(&currentState, parent->State, action->GetEffects());

		auto node = new Node(parent, parent->RunningCost + action->GetCost(), &currentState, action);

		if(GoalInState(goals, &currentState))
		{
			leaves->Add(node);
			foundOne = true;
		}
		else
		{
			TSet<AGoapAction*>* subset = new TSet<AGoapAction*>();
			ActionSubset(subset, usableActions, action);
			bool found = BuildGraph(node, leaves, subset, goals);
			delete subset;
		}

		delete(node);
	}
	return foundOne;
}

bool GoapPlanner::InState(TSet<TPair<FString, void*>>* test, TSet<TPair<FString, void*>>* state)
{
	for(auto t : *test)
	{
		bool match = false;
		for(auto s : *state)
		{
			//TODO check this eq
			if(s == t)
			{
				match = true;
				break;
			}
		}
		if(!match)
			return false;
	}
	return true;
}

bool GoapPlanner::GoalInState(TSet<TPair<FString, void*>>* test, TSet<TPair<FString, void*>>* state)
{
	for(auto t : *test)
	{ 
		for(auto s : *state)
		{
			if(t == s)
				return true;
		}
	}
	return false;
}

void GoapPlanner::PopulateState(TSet<TPair<FString, void*>>* result, TSet<TPair<FString, void*>>* currentState, TSet<TPair<FString, void*>>* stateChange)
{
	//TODO optimalize
	for(auto state : *currentState)
		result->Add(TPair<FString, void*>(state.Key, state.Value));

	for(auto change : *stateChange)
	{
		RemoveAllElementWithKey(result, change);
		result->Add(TPair<FString, void*>(change.Key, change.Value));
	}
}

void GoapPlanner::ActionSubset(TSet<AGoapAction*>* subset, TSet<AGoapAction*>* actions, AGoapAction* removeMe)
{
	for(auto action : *actions)
	{
		if(action != removeMe)
			subset->Add(action);
	}
}

Node::Node()
{
	Parent = nullptr;
	RunningCost = std::numeric_limits<float>::max();
	State = nullptr;
	Action = nullptr;
}

Node::Node(Node* parent, float runningCost, TSet<TPair<FString, void*>>* state, AGoapAction* action)
{
	Parent = parent;
	RunningCost = runningCost;
	State = state;
	Action = action;
}

Node::~Node()
{
	Parent = nullptr;
	RunningCost = std::numeric_limits<float>::max();
	State = new TSet<TPair<FString, void*>>();
	Action = nullptr;
}

void DeleteAllNotes(TArray<Node*>* notes)
{
	for(int32 i = notes->Num() - 1; i >= 0; --i)
	{
		auto note = (*notes)[i];
		delete(note);
		(*notes).RemoveAt(i, 1, false);
	}
}

void RemoveAllElementWithKey(TSet<TPair<FString, void*>>* set, TPair<FString, void*> newElement)
{	//TODO optimalize
	TArray<TPair<FString, void*>*> removableElements;
	for(auto pair : *set)
	{
		if(pair.Key.Equals(newElement.Key))
			removableElements.Add(&pair);
	}
	for(auto removable : removableElements)
	{
		set->Remove(*removable);
	}
}

