// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapNPC.h"
#include "GoapAgent.h"

// Sets default values
AGoapNPC::AGoapNPC() 
{
 	PrimaryActorTick.bCanEverTick = true;

	if(_goapAgent != nullptr)
		_goapAgent->RegisterDataProvider(this);

}
//
//void AGoapNPC::BeginPlay()
//{
//	Super::BeginPlay();
//	
//}
//
//void AGoapNPC::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//
//}

void AGoapNPC::GetWorldStates(TSet<TPair<FString, void*>>*)
{
	unimplemented();
}

void AGoapNPC::CreateGoalStates(TSet<TPair<FString, void*>>*)
{
	unimplemented();
}

void AGoapNPC::PlanFailed(TSet<TPair<FString, void*>>*)
{
	unimplemented();
}

void AGoapNPC::PlanFound(TSet<TPair<FString, void*>>* goals, TQueue<AGoapAction*>* actions)
{
	unimplemented();
}

void AGoapNPC::ActionsFinished()
{
	unimplemented();
}

void AGoapNPC::PlanAborted(AGoapAction* aborter)
{
	unimplemented();
}

bool AGoapNPC::MoveAgent(AGoapAction* nextAction)
{
	unimplemented();
	return false;
}

