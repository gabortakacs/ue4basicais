// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapAction.h"


// Sets default values
AGoapAction::AGoapAction()
{
 	PrimaryActorTick.bCanEverTick = true;
	_effects = new TSet<TPair<FString, void*>>();
	_preconditions = new TSet<TPair<FString, void*>>();
}

void AGoapAction::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGoapAction::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGoapAction::Reset()
{
	unimplemented();
}

bool AGoapAction::IsDone()
{
	unimplemented();
	return false;
}

bool AGoapAction::CheckProceduralPrecondition()
{
	unimplemented();
	return false;
}

bool AGoapAction::Perform()
{
	unimplemented();
	return false;
}

bool AGoapAction::RequiresInRange()
{
	unimplemented();
	return false;
}


void AGoapAction::AddEffect(FString key, void* value)
{
	_effects->Add(TPair<FString, void*>(key, value));
}

void AGoapAction::AddPrecondition(FString key, void* value)
{
	_preconditions->Add(TPair<FString, void*>(key, value));
}

AActor* AGoapAction::GetTarget() const
{
	return _target;
}

bool AGoapAction::IsInRange() const
{
	return _inRange;
}

TSet<TPair<FString, void*>>* AGoapAction::GetPreconditions() const
{
	return _preconditions;
}

TSet<TPair<FString, void*>>* AGoapAction::GetEffects() const
{
	return _effects;
}

float AGoapAction::GetCost() const
{
	return _cost;
}

