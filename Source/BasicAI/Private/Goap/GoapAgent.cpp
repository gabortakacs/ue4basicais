// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapAgent.h"
#include "GoapAction.h"
#include "GoapPlanner.h"
#include "IGoap.h"

void DeleteSetValuePointers(TSet<TPair<FString, void*>>* set);

// Sets default values
AGoapAgent::AGoapAgent()
{
 	PrimaryActorTick.bCanEverTick = true;
	_fsm = new Fsm();
	_availableActions = new TSet<AGoapAction*>();
	_currentActions = new TQueue<AGoapAction*>();
	_planner = new GoapPlanner();
	
	CreateStates();
}

AGoapAgent::~AGoapAgent()
{
	delete(_availableActions);
	delete(_currentActions);
	delete(_planner);
}

void AGoapAgent::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGoapAgent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	_fsm->Update(this);
}

AGoapAction* AGoapAgent::GetCurrentAction()
{
	return _currentAction;
}

void AGoapAgent::CreateStates()
{
	CreateIdleState();
	CreateMoveState();
	CreatePerformActionState();
}

void AGoapAgent::CreateIdleState()
{
	_idleState = [this](Fsm* fsm, AActor* actor)
	{
		TSet<TPair<FString, void*>>  worldStates;
		_dataProvider->GetWorldStates(&worldStates);
		TSet<TPair<FString, void*>>  goals;
		_dataProvider->CreateGoalStates(&goals);

		if(_planner->Plan(_currentActions, _availableActions, &worldStates, &goals))
		{
			_dataProvider->PlanFound(&goals, _currentActions);
			fsm->PopState();
			fsm->PushState(_performActionState);
		}
		else
		{
			_dataProvider->PlanFailed(&goals);
			fsm->PopState();
			fsm->PushState(_idleState);
		}
		DeleteSetValuePointers(&worldStates);
		DeleteSetValuePointers(&goals);
	};
}

void AGoapAgent::CreateMoveState()
{
	_moveToState = [this](Fsm* fsm, AActor* actor) -> void
	{
		_currentActions->Peek(_currentAction);
		if(!_currentAction->RequiresInRange() || _currentAction->GetTarget() == nullptr)
			return;

		fsm->PopState();
		fsm->PopState();
		fsm->PushState(_idleState);
	};
}

void AGoapAgent::CreatePerformActionState()
{
	_performActionState = [this](Fsm* fsm, AActor* actor) -> void
	{
		_currentActions->Peek(_currentAction);
		if(_currentAction->IsDone())
			_currentActions->Pop();

		if(!HasActionPlan())
		{
			fsm->PopState();
			fsm->PushState(_idleState);
			_dataProvider->ActionsFinished();
			return;
		}

		_currentActions->Peek(_currentAction);
		bool inRange = _currentAction->RequiresInRange() ? _currentAction->IsInRange() : true;
		
		if(!inRange)
		{
			fsm->PushState(_moveToState);
			return;
		}

		if(_currentAction->Perform())
			return;

		fsm->PopState();
		fsm->PushState(_idleState);
		CreateIdleState();
		_dataProvider->PlanAborted(_currentAction);
	};
}

void AGoapAgent::RegisterDataProvider(IGoap* dataProvider)
{
	_dataProvider = dataProvider;
}


void AGoapAgent::RegisterAvailableAction(AGoapAction* action)
{
	_availableActions->Add(action);
}

void AGoapAgent::UnregisterAvailableAction(AGoapAction* action)
{
	_availableActions->Remove(action);
}

bool AGoapAgent::HasActionPlan()
{
	return _currentActions->IsEmpty();
}


void DeleteSetValuePointers(TSet<TPair<FString, void*>>* set)
{
	for(auto s : *set)
	{
		delete(s.Value);
	}
}

