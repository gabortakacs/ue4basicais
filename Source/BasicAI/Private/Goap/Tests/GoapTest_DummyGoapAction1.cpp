// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTest_DummyGoapAction1.h"

// Sets default values
AGoapTest_DummyGoapAction1::AGoapTest_DummyGoapAction1()
{
 	PrimaryActorTick.bCanEverTick = true;
	AddEffect(FString("GoalState1"), new bool(true));
	_cost = 200;
}

void AGoapTest_DummyGoapAction1::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGoapTest_DummyGoapAction1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGoapTest_DummyGoapAction1::Reset()
{

}

bool AGoapTest_DummyGoapAction1::IsDone()
{
	return true;
}

bool AGoapTest_DummyGoapAction1::CheckProceduralPrecondition()
{
	return true;
}

bool AGoapTest_DummyGoapAction1::Perform()
{
	return true;
}

bool AGoapTest_DummyGoapAction1::RequiresInRange()
{
	return false;
}


