// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTest_DummyGoapAction3.h"

// Sets default values
AGoapTest_DummyGoapAction3::AGoapTest_DummyGoapAction3()
{
	PrimaryActorTick.bCanEverTick = true;
	AddEffect(FString("GoalState3"), new bool(true));
	_cost = 100;
}

void AGoapTest_DummyGoapAction3::BeginPlay()
{
	Super::BeginPlay();

}

void AGoapTest_DummyGoapAction3::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGoapTest_DummyGoapAction3::Reset()
{

}

bool AGoapTest_DummyGoapAction3::IsDone()
{
	return true;
}

bool AGoapTest_DummyGoapAction3::CheckProceduralPrecondition()
{
	return true;
}

bool AGoapTest_DummyGoapAction3::Perform()
{
	return true;
}

bool AGoapTest_DummyGoapAction3::RequiresInRange()
{
	return false;
}


