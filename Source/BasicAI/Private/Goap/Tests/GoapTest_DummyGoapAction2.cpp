// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTest_DummyGoapAction2.h"

// Sets default values
AGoapTest_DummyGoapAction2::AGoapTest_DummyGoapAction2()
{
	PrimaryActorTick.bCanEverTick = true;
	AddEffect(FString("GoalState2"), new bool(true));
	AddPrecondition(FString("GoalState3"), new bool(true));
	_cost = 99;
}

void AGoapTest_DummyGoapAction2::BeginPlay()
{
	Super::BeginPlay();

}

void AGoapTest_DummyGoapAction2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGoapTest_DummyGoapAction2::Reset()
{

}

bool AGoapTest_DummyGoapAction2::IsDone()
{
	return true;
}

bool AGoapTest_DummyGoapAction2::CheckProceduralPrecondition()
{
	return true;
}

bool AGoapTest_DummyGoapAction2::Perform()
{
	return true;
}

bool AGoapTest_DummyGoapAction2::RequiresInRange()
{
	return false;
}


