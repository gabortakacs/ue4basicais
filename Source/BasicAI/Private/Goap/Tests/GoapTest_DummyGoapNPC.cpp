// Fill out your copyright notice in the Description page of Project Settings.

#include "GoapTest_DummyGoapNPC.h"


// Sets default values
AGoapTest_DummyGoapNPC::AGoapTest_DummyGoapNPC()
{
 	PrimaryActorTick.bCanEverTick = true;

}
//
//void AGoapTest_DummyGoapNPC::BeginPlay()
//{
//	Super::BeginPlay();
//	
//}
//
//void AGoapTest_DummyGoapNPC::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//
//}

void AGoapTest_DummyGoapNPC::GetWorldStates(TSet<TPair<FString, void*>>* worldStates)
{
	worldStates->Add(TPair<FString, void*>(FString("IntAmount"), new int(10)));
	worldStates->Add(TPair<FString, void*>(FString("BoolAmount"), new bool(true)));
}

void AGoapTest_DummyGoapNPC::CreateGoalStates(TSet<TPair<FString, void*>>* goals)
{
	goals->Add(TPair<FString, void*>(FString("GoalState1"), new bool(true)));
	goals->Add(TPair<FString, void*>(FString("GoalState2"), new int(true)));
}

void AGoapTest_DummyGoapNPC::PlanFailed(TSet<TPair<FString, void*>>*)
{

}

void AGoapTest_DummyGoapNPC::PlanFound(TSet<TPair<FString, void*>>* goals, TQueue<AGoapAction*>* actions)
{

}

void AGoapTest_DummyGoapNPC::ActionsFinished()
{

}

void AGoapTest_DummyGoapNPC::PlanAborted(AGoapAction* aborter)
{

}

bool AGoapTest_DummyGoapNPC::MoveAgent(AGoapAction* nextAction)
{
	return true;
}
