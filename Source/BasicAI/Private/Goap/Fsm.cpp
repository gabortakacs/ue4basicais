#include "Fsm.h"


Fsm::Fsm()
{
}

void Fsm::Update(AActor* actor)
{
	auto fsmState = PeekState();
	if(fsmState == nullptr)
		return;

	fsmState(this, actor);
}

void Fsm::PushState(std::function<void(Fsm*, AActor*)> state)
{
	_stateStack.Add(state);
}

std::function<void(Fsm*, AActor*)> Fsm::PeekState()
{
	return _stateStack.Last();
}

void Fsm::PopState()
{
	_stateStack.Pop();
}

