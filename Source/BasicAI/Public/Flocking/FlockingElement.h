
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FlockingElement.generated.h"

class AFlockingManager;

UCLASS()
class BASICAI_API AFlockingElement : public AActor
{
	GENERATED_BODY()
private:
	UPROPERTY(EditAnywhere) float _speed;
	UPROPERTY(EditAnywhere) float _raycastStartPosOffsetMultplier;
	AFlockingManager* _flockingManager;

	FCollisionQueryParams* _traceParams;

	float _newSpeedTimer = 0.0f;

public:
	virtual void Tick(float) override;
	AFlockingElement();
	~AFlockingElement();
	void Init(AFlockingManager*);
	float GetSpeed();

private:
	bool IsTurning(float);
	bool CheckRaycast(FHitResult*);
	void MoveForward(float);
	void Rotate(FVector&, float);
	void SetNewSpeed();
	void ApplyRules(float);
};
