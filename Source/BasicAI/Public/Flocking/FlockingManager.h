
#pragma once



#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FlockingManager.generated.h"

class AFlockingElement;

UCLASS()
class BASICAI_API AFlockingManager : public AActor
{
	GENERATED_BODY()
private:
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") TArray<TSubclassOf<AFlockingElement>> _flockingElementBPs;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") int _flockElementAmount;

	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") FVector _flockingLimit;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") float _raycastDistance;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") float _avoidDistance;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") float _rotationSpeed;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") float _neightbourDistance;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") int _newGoalLocationChance;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") int _newGoalLocationChangeFrequency;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") FVector2D _speed;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") int _newSpeedChance;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") float _newSpeedChangeFrequency;
	UPROPERTY(EditAnywhere, Category = "Spawning Flocking settings") int _applyRulesChance;

	UPROPERTY(VisibleAnywhere, Category = "Spawning Flocking settings")FVector _goalLocation;
	TArray<AFlockingElement*> _flockElements;
	FBox _flockingBound;
	float _newGoalLocationTimer = 0.0f;

public:
	AFlockingManager();
	virtual void Tick(float DeltaTime) override;

	bool IsInBound(const FVector&) const;
	int GetApplyRulesChance() const;
	int GetNewSpeedChance() const;
	float GetNewSpeedChangeFrequency() const;
	float GetNeightbourDistance() const;
	float GetRotationSpeed() const;
	float GetRaycastDistance() const;
	float GetAvoidDistance() const;
	FVector GetGoalLocation() const;
	FVector2D GetSpeed() const;
	TArray<AFlockingElement*> GetElements() const;

protected:
	virtual void BeginPlay() override;

private:
	void SpawnFlockElements(int);
	AFlockingElement* SpawnFlockElement();
	void SetFlockingBound();
	void SetNewGoalLocation();
	FVector GetRandomPointInBound() const;

};
