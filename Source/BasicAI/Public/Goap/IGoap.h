// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class AGoapAction;
class AGoapAgent;

class BASICAI_API IGoap
{
public:
	IGoap();
	virtual ~IGoap() = 0;

	virtual void GetWorldStates(TSet<TPair<FString, void*>>*) = 0;
	virtual void CreateGoalStates(TSet<TPair<FString, void*>>*) = 0;
	virtual void PlanFailed(TSet<TPair<FString, void*>>*) = 0;
	virtual void PlanFound(TSet<TPair<FString, void*>>* goals, TQueue<AGoapAction*>* actions) = 0;
	virtual void ActionsFinished() = 0;
	virtual void PlanAborted(AGoapAction* aborter) = 0;
	virtual bool MoveAgent(AGoapAction* nextAction) = 0;
};
