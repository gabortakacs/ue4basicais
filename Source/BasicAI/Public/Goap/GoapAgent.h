// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Fsm.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GoapAgent.generated.h"

class AGoapAction;
class IGoap;
class GoapPlanner;

UCLASS()
class BASICAI_API AGoapAgent : public AActor
{
	GENERATED_BODY()
	
private:
	UPROPERTY(VisibleAnywhere, Category = "CurrentAction") AGoapAction* _currentAction;
	
	Fsm* _fsm;
	std::function<void(Fsm*, AActor*)> _idleState;
	std::function<void(Fsm*, AActor*)> _moveToState;
	std::function<void(Fsm*, AActor*)> _performActionState;

	TSet<AGoapAction*>* _availableActions;
	TQueue<AGoapAction*>* _currentActions;
	GoapPlanner* _planner;
	IGoap* _dataProvider;

public:	
	AGoapAgent();
	~AGoapAgent();
	virtual void Tick(float DeltaTime) override;

	AGoapAction* GetCurrentAction();

	void RegisterDataProvider(IGoap*);
	void RegisterAvailableAction(AGoapAction*);
	void UnregisterAvailableAction(AGoapAction*);

protected:
	virtual void BeginPlay() override;

private:
	void CreateStates();
	void CreateIdleState();
	void CreateMoveState();
	void CreatePerformActionState();

	bool HasActionPlan();
};
