// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapNPC.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GoapTest_DummyGoapNPC.generated.h"

UCLASS()
class BASICAI_API AGoapTest_DummyGoapNPC : public AGoapNPC
{
	GENERATED_BODY()

public:
	AGoapTest_DummyGoapNPC();
	//virtual void Tick(float DeltaTime) override;

	virtual void GetWorldStates(TSet<TPair<FString, void*>>*) override;
	virtual void CreateGoalStates(TSet<TPair<FString, void*>>*) override;
	virtual void PlanFailed(TSet<TPair<FString, void*>>*);
	virtual void PlanFound(TSet<TPair<FString, void*>>* goals, TQueue<AGoapAction*>* actions) override;
	virtual void ActionsFinished() override;
	virtual void PlanAborted(AGoapAction* aborter) override;
	virtual bool MoveAgent(AGoapAction* nextAction) override;



	//protected:
	//	virtual void BeginPlay() override;
};
