// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoapAction.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GoapTest_DummyGoapAction1.generated.h"


UCLASS()
class BASICAI_API AGoapTest_DummyGoapAction1 : public AGoapAction
{
	GENERATED_BODY()
	
public:	
	AGoapTest_DummyGoapAction1();
	virtual void Tick(float DeltaTime) override;

	virtual void Reset() override;
	virtual bool IsDone() override;
	virtual bool CheckProceduralPrecondition() override;
	virtual bool Perform() override;
	virtual bool RequiresInRange() override;
	

protected:
	virtual void BeginPlay() override;
};
