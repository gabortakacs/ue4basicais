#pragma once

#include <functional>
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

class BASICAI_API Fsm
{
//public:
//	typedef void(*Fsm::FsmState)(Fsm*, AActor*);

private:

	TArray<std::function<void(Fsm*, AActor*)>> _stateStack;
	std::function<void(Fsm*, AActor*)> PeekState();

public:
	Fsm();
	~Fsm();
	void Update(AActor*);
	void PushState(std::function<void(Fsm*, AActor*)>);
	void PopState();
};
