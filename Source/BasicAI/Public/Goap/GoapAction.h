// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GoapAction.generated.h"

class AGoapAgent;

UCLASS()
class BASICAI_API AGoapAction : public AActor
{
	GENERATED_BODY()
private:
	UPROPERTY(EditDefaultsOnly, Category = "GoapAction settings") AGoapAgent* _goapAgent;

protected:
	UPROPERTY(EditAnywhere, Category = "GoapAction settings") float _cost;

private:
	AActor* _target;
	bool _inRange;

	TSet<TPair<FString, void*>>* _effects;
	TSet<TPair<FString, void*>>* _preconditions;

public:	
	AGoapAction();
	virtual void Tick(float DeltaTime) override;
	virtual void Reset();
	virtual bool IsDone();
	virtual bool CheckProceduralPrecondition();
	virtual bool Perform();
	virtual bool RequiresInRange();

	void AddEffect(FString,void*);
	void AddPrecondition(FString, void*);

	bool IsInRange() const;
	float GetCost() const;
	AActor* GetTarget() const;
	TSet<TPair<FString, void*>>* GetPreconditions() const;
	TSet<TPair<FString, void*>>* GetEffects() const;


protected:
	virtual void BeginPlay() override;
};
