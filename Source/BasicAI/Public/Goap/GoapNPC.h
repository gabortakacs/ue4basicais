// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "IGoap.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GoapNPC.generated.h"

class AGoapAgent;

UCLASS()
class BASICAI_API AGoapNPC : public AActor, public IGoap
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly, Category = "GoapNPC Setting") AGoapAgent* _goapAgent;

public:	
	AGoapNPC();
	//virtual void Tick(float DeltaTime) override;

	virtual void GetWorldStates(TSet<TPair<FString, void*>>*) override;
	virtual void CreateGoalStates(TSet<TPair<FString, void*>>*) override;
	virtual void PlanFailed(TSet<TPair<FString, void*>>*) override;
	virtual void PlanFound(TSet<TPair<FString, void*>>* goals, TQueue<AGoapAction*>* actions) override;
	virtual void ActionsFinished() override;
	virtual void PlanAborted(AGoapAction* aborter) override;
	virtual bool MoveAgent(AGoapAction* nextAction) override;
	

//protected:
	//virtual void BeginPlay() override;
};
