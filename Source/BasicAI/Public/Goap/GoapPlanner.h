// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class AGoapAction;
class Node;
/**
 * 
 */
class BASICAI_API GoapPlanner
{
public:
	GoapPlanner();
	~GoapPlanner();
	bool Plan(TQueue<AGoapAction*>*, TSet<AGoapAction*>*, TSet<TPair<FString, void*>>*, TSet<TPair<FString, void*>>*);

private:
	void ResetActionsAndGetUsableActions(TSet<AGoapAction*>*, TSet<AGoapAction*>*);
	void PopulateState(TSet<TPair<FString, void*>>*, TSet<TPair<FString, void*>>*, TSet<TPair<FString, void*>>*);
	void ActionSubset(TSet<AGoapAction*>*, TSet<AGoapAction*>*, AGoapAction*);

	bool BuildGraph(Node*, TArray<Node*>*, TSet<AGoapAction*>*, TSet<TPair<FString, void*>>*);
	bool InState(TSet<TPair<FString, void*>>*, TSet<TPair<FString, void*>>*);
	bool GoalInState(TSet<TPair<FString, void*>>*, TSet<TPair<FString, void*>>*);
	void GetCheapestNode(Node*, TArray<Node*>*);
	void CreatePlan(TQueue<AGoapAction*>*, Node*);
};

class BASICAI_API Node
{
public:
	Node* Parent;
	float RunningCost;
	TSet<TPair<FString, void*>>* State;
	AGoapAction* Action;

public:
	Node();
	Node(Node* parent, float runningCost, TSet<TPair<FString, void*>>* state, AGoapAction* action);
	~Node();
};
